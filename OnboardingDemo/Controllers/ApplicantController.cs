﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnboardingDemo.Domain;

namespace OnboardingDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicantController : ControllerBase
    {

        [HttpPost]
        public void CreateCandidate([FromBody] string value)
        {
            var candidate = CandidateDossier.CreateStartingCandidate("Alexander", "Willemsen");
        }
        
    }
}