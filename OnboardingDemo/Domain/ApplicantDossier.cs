using System.Collections.Generic;

namespace OnboardingDemo.Domain
{
    public class ApplicantDossier
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Status Status { get; set; }
        public ApplicantType ApplicantType { get; set; }
        public IEnumerable<Phase> Phases { get; set; }

        internal ApplicantDossier(string firstName, string lastName, Status status, ApplicantType applicantType)
        {
            FirstName = firstName;
            LastName = lastName;
            Status = status;
            ApplicantType = applicantType;
        }

        protected static ApplicantDossier CreateStartingApplicant(string firstName, string lastName,
            ApplicantType applicantType)
        {
            return new ApplicantDossier(firstName, lastName, new Status(StatusEnum.Started), applicantType);
        }
    }
}