namespace OnboardingDemo.Domain
{
    public class Status
    {
        private int Id { get; set; }

        public Status(StatusEnum statusEnum)
        {
            Id = (int) statusEnum;
            StatusValue = statusEnum;
        }


        private StatusEnum StatusValue { get; set; }

        public void SetStatusChange(StatusEnum statusChange)
        {
            StatusValue = statusChange;
            Id = (int) statusChange;
        }
    }
}