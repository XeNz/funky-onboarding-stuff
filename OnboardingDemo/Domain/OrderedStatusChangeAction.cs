namespace OnboardingDemo.Domain
{
    public class OrderedStatusChangeAction
    {
        public int Id { get; set; }
        public StatusEnum StatusEnum { get; set; }
        public Action Action { get; set; }
        public int Order { get; set; }
    }
}