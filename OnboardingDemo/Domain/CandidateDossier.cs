using System.Buffers.Text;

namespace OnboardingDemo.Domain
{
    public class CandidateDossier : ApplicantDossier
    {
        private CandidateDossier(string firstName, string lastName, Status status) : base(
            firstName, lastName, status, ApplicantType.Candidate)
        {
        }


        internal static CandidateDossier CreateStartingCandidate(string firstName, string lastName) =>
            new CandidateDossier(firstName, lastName, new Status(StatusEnum.Started));
    }
}