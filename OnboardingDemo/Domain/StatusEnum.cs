namespace OnboardingDemo.Domain
{
    public enum StatusEnum
    {
        Started = 1,
        Ongoing = 2,
        Stopped = 3
    }
}