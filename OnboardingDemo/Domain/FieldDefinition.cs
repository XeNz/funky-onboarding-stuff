using System.Collections.Generic;

namespace OnboardingDemo.Domain
{
    public class FieldDefinition
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public FieldDataType FieldDataType { get; set; }
        public string DefaultValue { get; set; }
    }
}