using System.Collections.Generic;

namespace OnboardingDemo.Domain.Events
{
    public class StatusChangeEvent
    {
        public int DossierId { get; set; }
        public StatusEnum StatusFrom { get; set; } // or maybe instead of currentstatus -> nextstatus, try keywords like next and previous status idk
        public StatusEnum StatusTo { get; set; }
        public StatusType StatusType { get; set; }
        public IEnumerable<Action> ActionsToExecute { get; set; }
    }
}