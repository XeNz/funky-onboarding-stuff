namespace OnboardingDemo.Domain
{
    public class Action
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}