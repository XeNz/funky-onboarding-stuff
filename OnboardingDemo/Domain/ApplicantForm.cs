namespace OnboardingDemo.Domain
{
    //dossierthema
    public class ApplicantForm
    {
        public int Id { get; set; }
        public ApplicantDossier ApplicantDossier { get; set; }
        public Form Form { get; set; }
    }
}