namespace OnboardingDemo.Domain
{
    public enum ApplicantType
    {
        Candidate,
        JobStudent
    }
}