namespace OnboardingDemo.Domain
{
    public class FieldValue
    {
     public int Id { get; set; }
     public string Value { get; set; }
    }
}