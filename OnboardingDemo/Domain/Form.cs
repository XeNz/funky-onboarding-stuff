using System.Collections.Generic;

namespace OnboardingDemo.Domain
{
    //theme
    public class Form
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<FieldDefinition> FieldDefinitions { get; set; }
        public StatusEnum Status { get; set; }
        public bool Locked { get; set; }
    }
}