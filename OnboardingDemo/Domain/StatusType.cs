namespace OnboardingDemo.Domain
{
    public enum StatusType
    {
        PhaseStatus,
        DossierStatus
    }
}