namespace OnboardingDemo.Domain
{
    public enum FieldDataType
    {
        TEXT,
        INTEGER,
        DATE,
        OPTION
    }
}