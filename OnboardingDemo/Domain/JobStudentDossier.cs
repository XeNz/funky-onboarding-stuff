namespace OnboardingDemo.Domain
{
    public class JobStudentDossier : ApplicantDossier
    {
        internal JobStudentDossier(string firstName, string lastName, Status status) : base(firstName, lastName, status,
            ApplicantType.JobStudent)
        {
        }

        internal static JobStudentDossier CreateStartingCandidate(string firstName, string lastName) =>
            new JobStudentDossier(firstName, lastName, new Status(StatusEnum.Started));
    }
}