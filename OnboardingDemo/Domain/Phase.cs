namespace OnboardingDemo.Domain
{
    public class Phase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public StatusEnum StatusEnum { get; set; }
    }
}