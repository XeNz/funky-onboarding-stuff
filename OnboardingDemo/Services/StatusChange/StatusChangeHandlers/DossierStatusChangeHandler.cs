using System.Threading.Tasks;
using OnboardingDemo.Domain;
using OnboardingDemo.Domain.Events;

namespace OnboardingDemo.Services.StatusChange.StatusChangeHandlers
{
    public class DossierStatusChangeHandler : IStatusChangeHandler
    {
        public bool CanHandle(StatusType statusType) => statusType == StatusType.DossierStatus;

        public Task HandleStatusChange(StatusChangeEvent statusChangeEvent)
        {
            //handle dossier status change
            return Task.CompletedTask;
        }
    }
}