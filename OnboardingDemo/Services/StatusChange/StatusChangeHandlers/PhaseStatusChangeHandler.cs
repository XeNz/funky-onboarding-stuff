using System.Threading.Tasks;
using OnboardingDemo.Domain;
using OnboardingDemo.Domain.Events;

namespace OnboardingDemo.Services.StatusChange.StatusChangeHandlers
{
    public class PhaseStatusChangeHandler : IStatusChangeHandler
    {
        public bool CanHandle(StatusType statusType) => statusType == StatusType.PhaseStatus;


        public Task HandleStatusChange(StatusChangeEvent statusChangeEvent)
        {
            // handle the status change for a phase
            return Task.CompletedTask;
        }
    }
}