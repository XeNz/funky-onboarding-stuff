using System.Threading.Tasks;
using OnboardingDemo.Domain;
using OnboardingDemo.Domain.Events;

namespace OnboardingDemo.Services.StatusChange.StatusChangeHandlers
{
    public interface IStatusChangeHandler
    {
        bool CanHandle(StatusType statusType);
        Task HandleStatusChange(StatusChangeEvent statusChangeEvent);
    }
}