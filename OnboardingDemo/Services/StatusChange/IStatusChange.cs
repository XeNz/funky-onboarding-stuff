using System.Threading.Tasks;
using OnboardingDemo.Domain.Events;

namespace OnboardingDemo.Services.StatusChange
{
    public interface IStatusChange
    {
        Task ProcessStatusChange(StatusChangeEvent statusChangeEvent);
    }
}