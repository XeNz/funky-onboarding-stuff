using System.Collections.Generic;
using System.Threading.Tasks;
using OnboardingDemo.Domain.Events;
using OnboardingDemo.Services.StatusChange.StatusChangeHandlers;

namespace OnboardingDemo.Services.StatusChange
{
    public class StatusChangeService : IStatusChange
    {
        private readonly IEnumerable<IStatusChangeHandler> _statusChangeHandlers;

        public StatusChangeService(IEnumerable<IStatusChangeHandler> statusChangeHandlers)
        {
            _statusChangeHandlers = statusChangeHandlers;
        }

        public async Task ProcessStatusChange(StatusChangeEvent statusChangeEvent)
        {
            foreach (var handler in _statusChangeHandlers)
            {
                if (handler.CanHandle(statusChangeEvent.StatusType))
                {
                    await handler.HandleStatusChange(statusChangeEvent);
                }
            }
        }
    }
}