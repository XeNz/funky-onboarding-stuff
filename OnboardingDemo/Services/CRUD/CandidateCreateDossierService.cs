using OnboardingDemo.Controllers.DTO;
using OnboardingDemo.Domain;

namespace OnboardingDemo.Services.CRUD
{
    public class CandidateCreateDossierService : IApplicantDossierService<CandidateDossier, ApplicantDossierCrudRequest>
    {
        public CandidateDossier Validate(ApplicantDossierCrudRequest crudRequest)
        {
            throw new System.NotImplementedException();
        }

        public CandidateDossier Process(ApplicantDossierCrudRequest crudRequest)
        {
            var newCandidate = CandidateDossier.CreateStartingCandidate("Alex", "Will");
            // call repository .Add, in repository use automapper to map to entity
            return newCandidate;
        }

        public CandidateDossier PostProcess(ApplicantDossierCrudRequest crudRequest)
        {
            throw new System.NotImplementedException();
        }
    }
}