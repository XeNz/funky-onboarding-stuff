using OnboardingDemo.Controllers.DTO;
using OnboardingDemo.Domain;

namespace OnboardingDemo.Services.CRUD
{
    public interface IApplicantDossierService<out T, in Y>
        where T : ApplicantDossier
        where Y : ApplicantDossierCrudRequest
    {
        T Validate(Y crudRequest);
        T Process(Y crudRequest);
        T PostProcess(Y crudRequest);
    }
}