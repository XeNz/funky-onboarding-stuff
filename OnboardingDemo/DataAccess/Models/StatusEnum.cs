namespace OnboardingDemo.DataAccess.Models
{
    public enum StatusEnum
    {
        Started = 1,
        Ongoing = 2,
        Stopped = 3
    }
}