namespace OnboardingDemo.DataAccess.Models
{
    public enum ApplicantType
    {
        Candidate,
        JobStudent
    }
}