using System.Collections;
using System.Collections.Generic;

namespace OnboardingDemo.DataAccess.Models
{
    public class ApplicantDossier
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Status { get; set; }
        public ICollection<Phase> Phases { get; set; } 
    }
}