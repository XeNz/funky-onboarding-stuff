namespace OnboardingDemo.DataAccess.Models
{
    public enum StatusType
    {
        PhaseStatus,
        DossierStatus
    }
}