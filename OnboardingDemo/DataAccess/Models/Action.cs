namespace OnboardingDemo.DataAccess.Models
{
    public class Action
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}